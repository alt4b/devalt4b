<?php
// export total des données
define('EXPORT_FULL', 0);
// export des données filtrées
define('EXPORT_FILTER', 1);
// export de la page courante
define('EXPORT_PAGE', 2);
// export de la page courante
define('EXPORT_CSV', 3);


require './includes/init.php';
require './vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;

// TODO : VERIFIER LES AUTORISATIONS ICI



/**
* Certains modules ont une classe commune comme c_nomenclature ou c_societe
* Pour récupérer les données dans ce type de cas on utilise un ALIAS
*/

$aData = [];

try{
    if(isset($_GET['module']) && isset($_GET['type'])){
        ### Récupération des données
        $sModule = $_GET['module'];
        if(isset($_GET['alias'])){
            $sAlias = $_GET['alias']; // gestion nomenclature et societe
        }else{
            $sAlias='';
        }
        $sTemp = ($sAlias != '') ? $sAlias : $sModule;
        $sClassname = 'c_'.$sModule;

        $iType = (int)$_GET['type'];
        $sDate = date('d/m/Y ');

        $sType = '';

        // on vérifie que le module existe
        if(!class_exists($sClassname)){
            throw new Exception('Module non reconnu');
        }

        // on récupère les données selon le type d'export
        switch($iType){
            case EXPORT_FULL :
                $aData = $sClassname::export(-1, -1, false, $sAlias);
                $sType = 'all';
                break;

            case EXPORT_FILTER :
                $aData = $sClassname::export(-1, -1, true, $sAlias);
                $sType = 'filter';
                break;

            case EXPORT_PAGE :
                $iPage = (isset($_SESSION['pagination'][$sTemp])) ? (int)$_SESSION['pagination'][$sTemp]['page'] : 1;
                $iAmount = (isset($_SESSION['pagination'][$sTemp])) ? (int)$_SESSION['pagination'][$sTemp]['amount'] : 10;
                $aData = $sClassname::export($iPage, $iAmount, true, $sAlias);
                $sType = 'p'.$iPage;
                break;

            case EXPORT_CSV :
                $aData = $sClassname::export(-1, -1, true, $sAlias);
                $sType = 'csv';
                break;

            default :
                throw new Exception('Le type d\'export ('.$iType.') n\'est pas pris en charge');
        }

        $oUser = $_SESSION['user'];

        $oParam = new c_parametrage();
        $sNomFichier = 'export_'.$sTemp.'_'.$sType.'_'.date('d-m-Y').'_'.str_replace(' ', '_', strtolower(trim($oParam->getSSocieteNom())));


        ### Préparation du fichier
        //$oBoard = new PHPExcel();


        $oSpreadsheet = new Spreadsheet();

        $oSpreadsheet->getProperties()
            ->setCreator($oUser->getSNom() . ' ' . $oUser->getSPrenom())
            ->setTitle("Export - ".$sModule)
            ->setCompany($oParam->getSSocieteNom())
            ->setDescription("Export automatique des données de la base.");

        $oSpreadsheet->setActiveSheetIndex(0);
        $oSheet = $oSpreadsheet->getActiveSheet();
        $oSheet->setTitle($sModule);




        ### Ajout du contenu
        $row = 2;

        for($i = 0, $n = count($aData); $i < $n; $i++){
            $aArray = $aData[$i];

            $col = 'A';

            foreach($aArray as $key => $value){
                // création du header avec le titre
                if($i == 0){
                    $oSheet->setCellValue($col.'1', $key);

                    // les dimensions des colonnes s'adaptent en fonction du contenu
                    $oSheet->getColumnDimension($col)->setAutoSize(true);

                    // ajout du style pour le header

                    $oSheet->getStyle($col.'1')->applyFromArray([
                        'fill' => [
                            //'type' => Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => '459ac2')
                        ],
                        'font' => [
                            'bold' => true,
                            'color' => array('rgb' => '000000')
                        ]
                    ]);
                }

                // cellules de contenu
                $oSheet->setCellValue($col.$row, $value);
                $col++;
            }
            $row++;
        }

        if($sType=='csv'){
            header('Content-Type: text/csv; charset=utf-8');  
            header('Content-Disposition: attachment; filename="'.$sNomFichier.'.csv"');  
            $output = fopen("php://output", "w");  
            fputcsv($output, array('art_id'
                                    , 'art_ref'
                                    , 'art_pa_dol'
                                    , 'art_pa'
                                    , 'art_long'
                                    , 'art_larg'
                                    , 'art_haut'
                                    , 'art_poids'
                                    , 'art_long_bte'
                                    , 'art_larg_bte'
                                    , 'art_haut_bte'
                                    , 'art_bte_p_couche'
                                    , 'art_couche_p_colis'
                                    , 'art_qte_colis'
                                    , 'art_long_colis'
                                    , 'art_larg_colis'
                                    , 'art_haut_colis'
                                    , 'art_poids_colis'
                                    ), ';');       
            foreach ($aData as $key => $value) {
                fputcsv($output, $value, ';');
            } 
            fclose($output);
        }else{
            ### Exportation au format adéquat
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$sNomFichier.'.xlsx"');
            header('Cache-Control: max-age=0');

            // If you're serving to IE over SSL, then the following may be needed
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
            header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
            header('Pragma: public'); // HTTP/1.0


            $oWriter = IOFactory::createWriter($oSpreadsheet,'Xlsx');
            $oWriter->save('php://output');
            $oSpreadsheet->disconnectWorksheets();
            unset($oSpreadsheet);
        }
    }
}
catch(Exception $e){
    //print_r($e->getMessage());
    c_public::showFeedback('Une erreur est survenue', $e->getMessage());
}