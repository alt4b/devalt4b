<?php
/**
 * Administration de alt4b
 *
 * @copyright Copyright © Stéphan WAHL 2012
 * @version 1.0       30/04/12        01:32::00
 *
 * @author Stéphan WAHL
 * @since    alt4b 1.0
 *
 * @package alt4b
 * @subpackage Vues
 * @category Administration
 * @access protected
 * @uses db
 */
include './includes/init.php';
$sClass::moduleController($iDroit, $oLocation);

include './includes/header.php';

if($iDroit>=LECTURE){
    echo $sClass::moduleView($iDroit, $GLOBALS['_MODULE']);
}
else{
    echo c_public::getSMessageRefus();
}

include_once './includes/footer.php';
